"""Модель процессора
"""

import json
import logging
import sys

from isa import Opcode, AddressingType


class DataPath:
    """Data path"""

    def __init__(self, data_memory, input_buffer):
        self.data_memory = data_memory
        self.min_word = -9223372036854775808
        self.max_word = 9223372036854775807
        self.input_buffer = input_buffer
        self.output_buffer = []
        self.data_address = 0
        self.data = 0
        self.zero_flag = True
        self.acc = 0
        self.n_flag = 0

    def set_data(self, data):
        assert data >= self.min_word, "machine word is 32 bits"
        assert data <= self.max_word, "machine word is 32 bits"
        self.data = data

    def set_address(self, address):
        assert address >= self.min_word, "machine word is 32 bits"
        assert address <= self.max_word, "machine word is 32 bits"
        self.data_address = address

    def latch_memory(self):
        self.data_memory[self.data_address] = self.acc

    def latch_acc(self, instr):
        if instr["args"][0] != AddressingType.DIRECT_LOADING:
            self.acc = self.data_memory[self.data_address]
        else:
            self.acc = self.data

    def latch_data(self):
        self.data = self.data_memory[self.data_address]

    def acc_add(self):
        self.acc = self.acc + self.data

    def acc_sub(self):
        self.acc = self.acc - self.data

    def acc_div(self):
        self.acc = self.acc / self.data

    def acc_mod(self):
        self.acc = self.acc % self.data

    def set_zero_flag(self, value):
        self.zero_flag = value == 0

    def set_n_flag(self, value):
        self.n_flag = value < 0

    def out(self, type):
        if type == Opcode.OUT_INT:
            string = str(self.acc)
        elif type == Opcode.OUT_CHAR:
            string = chr(self.acc)
        self.output_buffer.append(string)

    def in_data_end(self):
        return len(self.input_buffer) == 0

    def get_in_data(self):
        return ord(self.input_buffer.pop(0))


class ControlUnit:
    """Processor model"""

    def __init__(self, program, data_path):
        self.program = program
        self.program_counter = 0
        self.data_path = data_path
        self._tick = 0

    def tick(self):
        self._tick += 1
        logging.debug(self)

    def current_tick(self):
        return self._tick

    def get_args(self, instr):
        if instr["args"]:
            addressing = instr["args"][0]
            value = instr["args"][1]
            if addressing == AddressingType.DIRECT:
                self.data_path.set_address(value)
                self.data_path.latch_data()
                self.tick()
                return self.data_path.data
            elif addressing == AddressingType.INDIRECT:
                self.data_path.set_address(value)
                self.data_path.latch_data()
                self.tick()
                self.data_path.set_address(self.data_path.data)
                self.data_path.latch_data()
                self.tick()
                return self.data_path.data
            elif addressing == AddressingType.DIRECT_LOADING:
                self.data_path.set_data(value)
                return value
        else:
            return None

    def latch_program_counter(self, sel_next):
        if sel_next:
            self.program_counter += 1
        else:
            instr = self.program[self.program_counter]
            arg = self.get_args(instr)
            self.program_counter = arg

    def decode_and_execute_instruction(self):
        instr = self.program[self.program_counter]
        opcode = instr["opcode"]
        logging.info("instruction: %s", instr)

        if opcode == Opcode.LD:
            self.get_args(instr)
            self.data_path.latch_acc(instr)
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.ST:
            self.data_path.set_address(self.get_args(instr))
            self.data_path.latch_memory()
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.IN:
            if self.data_path.in_data_end():
                raise EOFError
            self.data_path.acc = self.data_path.get_in_data()
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.OUT_INT:
            self.data_path.out(Opcode.OUT_INT)
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.OUT_CHAR:
            self.data_path.out(Opcode.OUT_CHAR)
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.HLT:
            raise StopIteration()
        elif opcode == Opcode.ADD:
            self.get_args(instr)
            self.data_path.acc_add()
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.SUB:
            self.get_args(instr)
            self.data_path.acc_sub()
            self.data_path.set_zero_flag(self.data_path.acc)
            self.data_path.set_n_flag(self.data_path.acc)
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.DIV:
            self.get_args(instr)
            self.data_path.acc_div()
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.MOD:
            self.get_args(instr)
            self.data_path.acc_mod()
            self.data_path.set_zero_flag(self.data_path.acc)
            self.data_path.set_n_flag(self.data_path.acc)
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.CMP:
            second_val = self.get_args(instr)
            self.data_path.set_zero_flag(self.data_path.acc - second_val)
            self.data_path.set_n_flag(self.data_path.acc - second_val)
            self.tick()
            self.latch_program_counter(sel_next=True)
            self.tick()
        elif opcode == Opcode.JMP:
            self.latch_program_counter(False)
            self.tick()
        elif opcode == Opcode.JE:
            self.latch_program_counter(not self.data_path.zero_flag)
            self.tick()
        elif opcode == Opcode.JNE:
            self.latch_program_counter(self.data_path.zero_flag)
            self.tick()
        elif opcode == Opcode.JGE:
            self.latch_program_counter(not (self.data_path.zero_flag or not self.data_path.n_flag))
            self.tick()

    def __repr__(self):
        state = "{{TICK: {}, PC: {}, DR: {}, ADDR: {}, ACC: {}}}".format(
            self._tick,
            self.program_counter,
            self.data_path.data,
            self.data_path.data_address,
            self.data_path.acc,
        )

        return state


def simulation(code, input_tokens, data_memory_size, limit):
    data_section = []
    text_section = []
    for element in code:
        if not isinstance(element, dict):
            data_section.append(element)
        else:
            text_section.append(element)
    data_memory = [0] * data_memory_size
    for number, value in enumerate(data_section):
        data_memory[number] = value
    data_path = DataPath(data_memory, input_tokens)
    control_unit = ControlUnit(text_section, data_path)
    instr_counter = 0
    try:
        while True:
            assert limit > instr_counter, "too long execution, increase limit!"
            control_unit.decode_and_execute_instruction()
            instr_counter += 1
    except StopIteration:
        pass
    except EOFError:
        logging.warning('Input buffer is empty!')

    return ''.join(data_path.output_buffer), instr_counter, control_unit.current_tick()


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: machine.py <code_file> <input_file>"
    code, input = args
    with open(code, "rt", encoding="utf-8") as f:
        code = f.read()
    code = json.loads(code)
    with open(input, "rt", encoding="utf-8") as f:
        input = f.read()

    input = list(input)
    output, instr_counter, ticks = simulation(code, input, data_memory_size=100, limit=10000)
    print('output: ', ''.join(output))
    print("instr_counter: ", instr_counter, "ticks:", ticks)
    return output


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
