"""Instruction set architecture
"""


import json

from enum import Enum


class AddressingType(int, Enum):
    '''Режимы адресации'''
    DIRECT = 0
    INDIRECT = 1
    DIRECT_LOADING = 2


class Opcode(str, Enum):
    '''Команды'''
    IN = 'in'
    OUT_INT = 'out_int'
    OUT_CHAR = 'out_char'
    HLT = 'hlt'
    ST = 'st'
    LD = 'ld'
    ADD = 'add'
    DIV = 'div'
    MOD = 'mod'
    CMP = 'cmp'
    JMP = 'jmp'
    JE = 'je'
    JNE = 'jne'
    JGE = 'jge'
    SUB = 'sub'


COMMANDS = {
    'jmp': {"opcode": Opcode.JMP, "args_count": 1},
    'je': {"opcode": Opcode.JE, "args_count": 1},
    'jne': {"opcode": Opcode.JNE, "args_count": 1},
    'jge': {"opcode": Opcode.JGE, "args_count": 1},
    'in': {"opcode": Opcode.IN, "args_count": 0},
    'out_int': {"opcode": Opcode.OUT_INT, "args_count": 0},
    'out_char': {"opcode": Opcode.OUT_CHAR, "args_count": 0},
    'hlt': {"opcode": Opcode.HLT, "args_count": 0},
    'ld': {"opcode": Opcode.LD, "args_count": 1},
    'add': {"opcode": Opcode.ADD, "args_count": 1},
    'sub': {"opcode": Opcode.SUB, "args_count": 1},
    'mod': {"opcode": Opcode.MOD, "args_count": 1},
    'div': {"opcode": Opcode.DIV, "args_count": 1},
    'st': {"opcode": Opcode.ST, "args_count": 1},
    'cmp': {"opcode": Opcode.CMP, "args_count": 1},
}


def write_code(filename, code):
    """Записать машинный код в файл."""
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))


def read_code(filename):
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())
    return code
