"""Тесты для модели транслятора
"""
import contextlib
import io
import os
import tempfile
import unittest
import pytest
import machine
import translator


@pytest.mark.golden_test("golden/*.yml")
def test_whole_by_golden(golden):
    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.asm")
        input_stream = os.path.join(tmpdirname, "input")
        target = os.path.join(tmpdirname, "target")

        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden["input"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source, target])
            print("============================================================")
            machine.main([target, input_stream])

        with open(target, encoding="utf-8") as file:
            code = file.read()

        assert code == golden.out["code"]
        assert stdout.getvalue() == golden.out["output"]


def machine_run(data_memory, input_tokens, text_section, limit=10000):
    data_path = machine.DataPath(data_memory, input_tokens)
    control_unit = machine.ControlUnit(text_section, data_path)
    instr_counter = 0
    try:
        while True:
            assert limit > instr_counter, "too long execution, increase limit!"
            control_unit.decode_and_execute_instruction()
            instr_counter += 1
    except StopIteration:
        return control_unit


class MachineTest(unittest.TestCase):
    """Machine tests"""

    def test_zero_flag(self):
        intstructions = [{
            "opcode": "ld",
            "args": [
                2,
                14
            ]
        }, {
            "opcode": "cmp",
            "args": [
                2,
                0
            ]
        }, {
            "opcode": "je",
            "args": [
                2,
                5
            ]
        }, {
            "opcode": "hlt",
            "args": None
        }]

        self.assertEqual(machine_run([], [], intstructions, limit=10000).data_path.acc, 14)
