section .code
    start:
        in
        cmp #0
        je .end
        out_char
        jmp .start
    end:
        hlt
