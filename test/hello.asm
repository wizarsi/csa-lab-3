section .data
    greeting: string "Hello, world!"
    null_term: number 0
    current: number 0
section .code
    start:
        ld (current)
        cmp #0
        je .end
        out_char
        ld current
        add #1
        st current
        jmp .start
    end:
        hlt
