section .data
	a: number 1
	b: number 0
	c: number 0
	sum: number 0

section .code
	start:
		ld c
		sub #4000000
		jge .end
		ld a
		add b
		st c
		ld b
		st a
		ld c
		st b
		mod #2
		jne .start
		ld b
		add sum
		st sum
		jmp .start
	end:
		ld sum
		out_int
		hlt
