* Васильев Андрей P33121
* `asm | acc | harv | hw | tick | struct | stream | port | prob2`

## Язык программирования

```
<program> ::= <program> <line> | <line>
<line> ::=  <opcode_without_arg> | <opcode_transfer> " ."<term> 
    | <opcode_arg> " (" <term> ")" | <opcode_arg> " #"<number> 
    | <opcode_arg> <term>
    | <label> | <comment> | <line> <comment>
    |  <label> " number " <number> | <label> ' string ' '"' <term> '"'
    | "section ."<term> | "\n" 

<label> ::= <term>":"
<opcode_without_arg> ::= "in" | "out_int" | "hlt" | "out_char"
<opcode_arg> ::= "add" | "div" | "mul" | "mod"| "cmp" | "st" | "ld" | "sub"
<opcode_transfer> ::= "jmp" | "je" | "jge" | "jne"

<term> ::= <term> <term> | <number> | <letter>
<letter> ::= [a-z] | [A-Z]
<number> ::= [1-9] | <number> <number>
<comment> ::=  ";"<term>
```
Описание:
* Поддержка меток
* В коде две секции .data, .code(данные, инструкции)
* 2 типа данных для строк string и для чисел number
* Ввод-вывод через команды out_int, out_char

Операнды:

* \#number прямая загрузка
* (метка) косвенная адресация
* метка прямая адресация

## Организация памяти

    1. Память команд. Машинное слово -- не определено
    2. Память данных. Машинное слово -- 32 бит, знаковое. Линейное адресное пространство. Реализуется списком чисел

## Система команд

| Syntax     | Кол-во тактов | Comment                                                              |
|:-----------|--------------|:---------------------------------------------------------------------|
| `in`       | 2            | `запись в acumulator из ус-ва ввода`                                 |
| `out_int`  | 2            | `запись в стандартный поток вывода из acumulator(числа)`             |
| `out_char` | 2            | `запись в стандартный поток вывода из acumulator(символ)`            |
| `hlt`      | 0            | `оставновка процессора`                                              |
| `add`      | 2            | `прибавить к acumulator операнд`                                     |
| `sub`      | 2            | `вычесть из acumulator операнд`                                      |
| `div`      | 2            | `поделить acumulator на операнд`                                     |
| `mul`      | 2            | `значение из acumulator умножается на операнд`                       |
| `mod`      | 2            | `остаток от деления acumulator`                                      |
| `cmp`      | 1            | `выставить флаги по вычитанию из acumulator(регистр не обновляется)` |
| `st`       | 2            | `сохранить accumulator в память`                                     |
| `ld`       | 2            | `загрузить значение в acumulator по аргументу`                       |
| `jmp`      | 1            | `переход на метку`                                                   |
| `je`       | 1            | `переход на метку если z=1`                                          |
| `jne`       | 1            | `переход на метку если z != 1`                                       |
| `jge`       | 1            | `переход на метку если z = 1 или n = 0`                              |

### Кодирование инструкций

* Инструкции с данными последоватально хранятся в json файле
* Так как архитектура гарвардская перед запуском модели процессора данные и инструкции отделяются друг от друга в памяти

## Транслятор

Исходный код prob2.asm:

```
section .data
	a: number 1
	b: number 0
	c: number 0
	sum: number 0

section .code
	start:
		ld c
		sub #4000000
		jge .end
		ld a
		add b
		st c
		ld b
		st a
		ld c
		st b
		mod #2
		jne .start
		ld b
		add sum
		st sum
		jmp .start
	end:
		ld sum
		out_int
		hlt
```

Результат трансляции файл в json формате prob2_result:

```
[
    1,
    0,
    0,
    0,
    {
        "opcode": "ld",
        "args": [
            0,
            2
        ]
    },
    {
        "opcode": "sub",
        "args": [
            2,
            4000000
        ]
    },
    {
        "opcode": "jge",
        "args": [
            2,
            16
        ]
    },
    {
        "opcode": "ld",
        "args": [
            0,
            0
        ]
    },
    {
        "opcode": "add",
        "args": [
            0,
            1
        ]
    },
    {
        "opcode": "st",
        "args": [
            2,
            2
        ]
    },
    {
        "opcode": "ld",
        "args": [
            0,
            1
        ]
    },
    {
        "opcode": "st",
        "args": [
            2,
            0
        ]
    },
    {
        "opcode": "ld",
        "args": [
            0,
            2
        ]
    },
    {
        "opcode": "st",
        "args": [
            2,
            1
        ]
    },
    {
        "opcode": "mod",
        "args": [
            2,
            2
        ]
    },
    {
        "opcode": "jne",
        "args": [
            2,
            0
        ]
    },
    {
        "opcode": "ld",
        "args": [
            0,
            1
        ]
    },
    {
        "opcode": "add",
        "args": [
            0,
            3
        ]
    },
    {
        "opcode": "st",
        "args": [
            2,
            3
        ]
    },
    {
        "opcode": "jmp",
        "args": [
            2,
            0
        ]
    },
    {
        "opcode": "ld",
        "args": [
            0,
            3
        ]
    },
    {
        "opcode": "out_int",
        "args": null
    },
    {
        "opcode": "hlt",
        "args": null
    }
]
```

## Модель процессора

![processor model](processor.png)

* program_counter - регистр, где хранится адресс следующей выполняемой команды
* data_address - хранится адресс, по-которому нужно достать данные
* data - регистр, где хранятся данные, вытаскиваемые из памяти
* процессор акумуляторного типа, все вычисления через регистр acc
* машинное слово 32 бита
* Ввод-вывод через порты input, output

Журнал работы процессора на примере cat.asm:

```
INFO:root:instruction: {'opcode': 'in', 'args': None}
DEBUG:root:{TICK: 1, PC: 0, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 2, PC: 1, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'cmp', 'args': [2, 0]}
DEBUG:root:{TICK: 3, PC: 1, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 4, PC: 2, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'je', 'args': [2, 5]}
DEBUG:root:{TICK: 5, PC: 3, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'out_char', 'args': None}
DEBUG:root:{TICK: 6, PC: 3, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 7, PC: 4, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'jmp', 'args': [2, 0]}
DEBUG:root:{TICK: 8, PC: 0, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'in', 'args': None}
DEBUG:root:{TICK: 9, PC: 0, DR: 0, ADDR: 0, ACC: 101}
DEBUG:root:{TICK: 10, PC: 1, DR: 0, ADDR: 0, ACC: 101}
INFO:root:instruction: {'opcode': 'cmp', 'args': [2, 0]}
DEBUG:root:{TICK: 11, PC: 1, DR: 0, ADDR: 0, ACC: 101}
DEBUG:root:{TICK: 12, PC: 2, DR: 0, ADDR: 0, ACC: 101}
INFO:root:instruction: {'opcode': 'je', 'args': [2, 5]}
DEBUG:root:{TICK: 13, PC: 3, DR: 0, ADDR: 0, ACC: 101}
INFO:root:instruction: {'opcode': 'out_char', 'args': None}
DEBUG:root:{TICK: 14, PC: 3, DR: 0, ADDR: 0, ACC: 101}
DEBUG:root:{TICK: 15, PC: 4, DR: 0, ADDR: 0, ACC: 101}
INFO:root:instruction: {'opcode': 'jmp', 'args': [2, 0]}
DEBUG:root:{TICK: 16, PC: 0, DR: 0, ADDR: 0, ACC: 101}
INFO:root:instruction: {'opcode': 'in', 'args': None}
DEBUG:root:{TICK: 17, PC: 0, DR: 0, ADDR: 0, ACC: 115}
DEBUG:root:{TICK: 18, PC: 1, DR: 0, ADDR: 0, ACC: 115}
INFO:root:instruction: {'opcode': 'cmp', 'args': [2, 0]}
DEBUG:root:{TICK: 19, PC: 1, DR: 0, ADDR: 0, ACC: 115}
DEBUG:root:{TICK: 20, PC: 2, DR: 0, ADDR: 0, ACC: 115}
INFO:root:instruction: {'opcode': 'je', 'args': [2, 5]}
DEBUG:root:{TICK: 21, PC: 3, DR: 0, ADDR: 0, ACC: 115}
INFO:root:instruction: {'opcode': 'out_char', 'args': None}
DEBUG:root:{TICK: 22, PC: 3, DR: 0, ADDR: 0, ACC: 115}
DEBUG:root:{TICK: 23, PC: 4, DR: 0, ADDR: 0, ACC: 115}
INFO:root:instruction: {'opcode': 'jmp', 'args': [2, 0]}
DEBUG:root:{TICK: 24, PC: 0, DR: 0, ADDR: 0, ACC: 115}
INFO:root:instruction: {'opcode': 'in', 'args': None}
DEBUG:root:{TICK: 25, PC: 0, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 26, PC: 1, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'cmp', 'args': [2, 0]}
DEBUG:root:{TICK: 27, PC: 1, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 28, PC: 2, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'je', 'args': [2, 5]}
DEBUG:root:{TICK: 29, PC: 3, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'out_char', 'args': None}
DEBUG:root:{TICK: 30, PC: 3, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 31, PC: 4, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'jmp', 'args': [2, 0]}
DEBUG:root:{TICK: 32, PC: 0, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'in', 'args': None}
DEBUG:root:{TICK: 33, PC: 0, DR: 0, ADDR: 0, ACC: 32}
DEBUG:root:{TICK: 34, PC: 1, DR: 0, ADDR: 0, ACC: 32}
INFO:root:instruction: {'opcode': 'cmp', 'args': [2, 0]}
DEBUG:root:{TICK: 35, PC: 1, DR: 0, ADDR: 0, ACC: 32}
DEBUG:root:{TICK: 36, PC: 2, DR: 0, ADDR: 0, ACC: 32}
INFO:root:instruction: {'opcode': 'je', 'args': [2, 5]}
DEBUG:root:{TICK: 37, PC: 3, DR: 0, ADDR: 0, ACC: 32}
INFO:root:instruction: {'opcode': 'out_char', 'args': None}
DEBUG:root:{TICK: 38, PC: 3, DR: 0, ADDR: 0, ACC: 32}
DEBUG:root:{TICK: 39, PC: 4, DR: 0, ADDR: 0, ACC: 32}
INFO:root:instruction: {'opcode': 'jmp', 'args': [2, 0]}
DEBUG:root:{TICK: 40, PC: 0, DR: 0, ADDR: 0, ACC: 32}
INFO:root:instruction: {'opcode': 'in', 'args': None}
DEBUG:root:{TICK: 41, PC: 0, DR: 0, ADDR: 0, ACC: 99}
DEBUG:root:{TICK: 42, PC: 1, DR: 0, ADDR: 0, ACC: 99}
INFO:root:instruction: {'opcode': 'cmp', 'args': [2, 0]}
DEBUG:root:{TICK: 43, PC: 1, DR: 0, ADDR: 0, ACC: 99}
DEBUG:root:{TICK: 44, PC: 2, DR: 0, ADDR: 0, ACC: 99}
INFO:root:instruction: {'opcode': 'je', 'args': [2, 5]}
DEBUG:root:{TICK: 45, PC: 3, DR: 0, ADDR: 0, ACC: 99}
INFO:root:instruction: {'opcode': 'out_char', 'args': None}
DEBUG:root:{TICK: 46, PC: 3, DR: 0, ADDR: 0, ACC: 99}
DEBUG:root:{TICK: 47, PC: 4, DR: 0, ADDR: 0, ACC: 99}
INFO:root:instruction: {'opcode': 'jmp', 'args': [2, 0]}
DEBUG:root:{TICK: 48, PC: 0, DR: 0, ADDR: 0, ACC: 99}
INFO:root:instruction: {'opcode': 'in', 'args': None}
DEBUG:root:{TICK: 49, PC: 0, DR: 0, ADDR: 0, ACC: 97}
DEBUG:root:{TICK: 50, PC: 1, DR: 0, ADDR: 0, ACC: 97}
INFO:root:instruction: {'opcode': 'cmp', 'args': [2, 0]}
DEBUG:root:{TICK: 51, PC: 1, DR: 0, ADDR: 0, ACC: 97}
DEBUG:root:{TICK: 52, PC: 2, DR: 0, ADDR: 0, ACC: 97}
INFO:root:instruction: {'opcode': 'je', 'args': [2, 5]}
DEBUG:root:{TICK: 53, PC: 3, DR: 0, ADDR: 0, ACC: 97}
INFO:root:instruction: {'opcode': 'out_char', 'args': None}
DEBUG:root:{TICK: 54, PC: 3, DR: 0, ADDR: 0, ACC: 97}
DEBUG:root:{TICK: 55, PC: 4, DR: 0, ADDR: 0, ACC: 97}
INFO:root:instruction: {'opcode': 'jmp', 'args': [2, 0]}
DEBUG:root:{TICK: 56, PC: 0, DR: 0, ADDR: 0, ACC: 97}
INFO:root:instruction: {'opcode': 'in', 'args': None}
DEBUG:root:{TICK: 57, PC: 0, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 58, PC: 1, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'cmp', 'args': [2, 0]}
DEBUG:root:{TICK: 59, PC: 1, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 60, PC: 2, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'je', 'args': [2, 5]}
DEBUG:root:{TICK: 61, PC: 3, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'out_char', 'args': None}
DEBUG:root:{TICK: 62, PC: 3, DR: 0, ADDR: 0, ACC: 116}
DEBUG:root:{TICK: 63, PC: 4, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'jmp', 'args': [2, 0]}
DEBUG:root:{TICK: 64, PC: 0, DR: 0, ADDR: 0, ACC: 116}
INFO:root:instruction: {'opcode': 'in', 'args': None}
WARNING:root:Input buffer is empty!
output:  test cat
instr_counter:  40 ticks: 64
```

## Апробация

| ФИО                | Алгоритм | LoC | code байт | code инстр | инстр. | такт |
|--------------------|----------|-----|-----------|------------|--------|------|
| Васильев Андрей    | hello    | 16  | -         | 9          | 107    | 228  |
| Васильев Андрей    | cat      | 9   | -         | 6          | 40     | 64   |
| Васильев Андрей    | prob2    | 28  | -         | 19         | 457    | 1028 |
