"""Транслятор
"""

import sys
import re

from isa import write_code, COMMANDS, AddressingType, Opcode


def translate(text):
    text = [re.sub(r';.*$', '', line) for line in text]
    text = [line.strip() for line in text]
    text = [line for line in text if line]
    program = ''
    for line in text:
        program += '\n' + line

    section_data = program.find('section .data')
    section_code = program.find('section .code')
    assert section_code != -1, ".code section not found"
    assert section_data == -1 or section_data < section_code, ".data section must be first"
    code_start = section_code + len('section .code') + 1
    if section_data != -1:
        data_start = section_data + len('section .data') + 1
        data = program[data_start: section_code - 1].split('\n')
    else:
        data = []
    code = program[code_start:].split('\n')

    machine_code = []
    labels_data_section = {}
    for line in data:
        label_name, string = line.split(':', 1)
        if string.find(' string "') != -1:
            labels_data_section[label_name] = len(machine_code)
            data_line = str(string[len(' string "'):-1])
            for term in data_line:
                machine_code.append(ord(term))
        elif string.find(' number ') != -1:
            number = int(string[len(' number '):])
            machine_code.append(number)
            labels_data_section[label_name] = len(machine_code) - 1
        else:
            assert True, "wrong data type in .data section"

    labels_code_section = {}
    commands = []
    for line in code:
        if ':' in line:
            label = line.split(':', 1)[0].strip()
            labels_code_section[label] = len(commands)
        else:
            parts = line.split()
            op = parts[0]
            command = COMMANDS[op]
            assert len(parts) - 1 == command["args_count"], \
                'incorrect number of arguments in command'

            operand = None
            if len(parts) == 2:
                command_operand = parts[1]
                is_label = re.match(r'\.?[A-Za-z1-9_]+', command_operand)
                is_indirect = re.match(r'\([A-Za-z1-9_]+\)', command_operand)
                if command_operand[0] == '#':
                    operand = [AddressingType.DIRECT_LOADING, int(command_operand[1:])]
                elif is_label or is_indirect:
                    operand = [None, command_operand]
            commands.append({"opcode": op, "args": operand})

    for i in commands:
        if i["args"] and i["args"][0] is None:
            command_opcode = COMMANDS[i["opcode"]]["opcode"]
            if COMMANDS[i["opcode"]]["args_count"] == 1 and command_opcode in (Opcode.JMP,
                                                                               Opcode.JE,
                                                                               Opcode.JGE,
                                                                               Opcode.JNE):
                assert i["args"][1][0] == '.', 'in a transfer command, the label starts with .'
                op = labels_code_section[i["args"][1][1:]]
                args = [AddressingType.DIRECT, op]
                machine_code.append(
                    {"opcode": i["opcode"], "args": args})
            else:
                if i["args"][1][0] == '(' and i["args"][1][-1] == ')':
                    op = labels_data_section[i["args"][1][1:-1]]
                    args = [AddressingType.INDIRECT, op]
                    machine_code.append(
                        {"opcode": i["opcode"], "args": args})
                else:
                    op = labels_data_section[i["args"][1]]
                    args = [AddressingType.DIRECT, op]
                    machine_code.append(
                        {"opcode": i["opcode"], "args": args})

            if machine_code[-1]["opcode"] == Opcode.ST or machine_code[-1]["opcode"] == Opcode.JMP \
                    or machine_code[-1]["opcode"] == Opcode.JE \
                    or machine_code[-1]["opcode"] == Opcode.JGE \
                    or command_opcode == Opcode.JNE:
                if machine_code[-1]["args"][0] == AddressingType.DIRECT:
                    args = [AddressingType.DIRECT_LOADING, op]
                    machine_code[-1] = {"opcode": i["opcode"], "args": args}
                elif machine_code[-1]["args"][0] == AddressingType.INDIRECT:
                    args = [AddressingType.DIRECT, op]
                    machine_code[-1] = {"opcode": i["opcode"], "args": args}
        else:
            machine_code.append(i)

    return machine_code


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <input_file> <target_file>"
    source, target = args
    with open(source, "rt", encoding="utf-8") as f:
        source = f.readlines()

    code = translate(source)
    write_code(target, code)


if __name__ == '__main__':
    main(sys.argv[1:])
